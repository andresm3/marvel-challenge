package com.dev.marvel;

import com.dev.marvel.model.Character;
import com.dev.marvel.model.CharactersData;
import com.dev.marvel.model.CharactersResponse;
import com.dev.marvel.service.MarvelApiClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@SpringBootTest
public class MarvelApiClientTest {

	@Mock
	private RestTemplate restTemplate;
	@Autowired
	private MarvelApiClient marvelApiClient;

	private List<Character>listOfCharacters;
	private List<Integer>listOfIds;
	@BeforeEach
	public void setup() {
		listOfCharacters = List.of(
				new Character(1011334,"3-D Man","","2014-04-29T14:18:17-0400",
						"http://gateway.marvel.com/v1/public/characters/1011334"),
				new Character(1009144,"A.I.M.","AIM is a terrorist organization bent on destroying " +
						"the world.","2013-10-17T14:41:30-0400",
						"http://gateway.marvel.com/v1/public/characters/1009144"),
				new Character(1010699,"Aaron Stack","","1969-12-31T19:00:00-0500",
						"http://gateway.marvel.com/v1/public/characters/1010699"),
				new Character(1011297,"Agent Brand","","2013-10-24T13:09:30-0400",
						"http://gateway.marvel.com/v1/public/characters/1011297")
		);
		listOfIds = List.of(1011334,1009144,1010699,1011297);
	}

	@Test
	public void testGetAllCharacters() {
		String apiUrl = marvelApiClient.createEndpoint("characters?");

		CharactersResponse mockResponse = new CharactersResponse();
		CharactersData charactersData = new CharactersData();
		List<Character> characters = new ArrayList<>();
		charactersData.setResults(characters);
		mockResponse.setData(charactersData);

		when(restTemplate.getForObject(apiUrl, CharactersResponse.class)).thenReturn(mockResponse);

		List<Character> result = marvelApiClient.getAllCharacters();
		assertEquals(result.stream().distinct().filter(e->listOfIds.contains(e.getId())).collect(Collectors.toList()).size(), 4);
	}

	@Test
	public void testGetCharacterById() {
		int characterId = 1011334;
		String apiUrl = marvelApiClient.createEndpoint("characters/" + characterId + "?");

		Character mockCharacter = new Character(1011334, "3-D Man", "", "2014-04-29T14:18:17-0400",
				"http://gateway.marvel.com/v1/public/characters/1011334");

		when(restTemplate.getForObject(apiUrl, Character.class)).thenReturn(mockCharacter);

		Character result = marvelApiClient.getCharacterById(characterId);
		assertNotNull(result);
		assertEquals(result.getId(), mockCharacter.getId());
		assertEquals(result.getName(), mockCharacter.getName());
		assertEquals(result.getDescription(), mockCharacter.getDescription());
		assertEquals(result.getModified(), mockCharacter.getModified());
		assertEquals(result.getResourceURI(), mockCharacter.getResourceURI());
	}
}