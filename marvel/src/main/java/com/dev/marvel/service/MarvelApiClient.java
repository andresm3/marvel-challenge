package com.dev.marvel.service;

import com.dev.marvel.model.Character;
import com.dev.marvel.model.CharactersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

@Service
public class MarvelApiClient {

    private static final String BASE_URL = "https://gateway.marvel.com/v1/public/";
    private static final String API_KEY = "ce0299f62eb62cef0d96fc3bde0c731c";
    private static final String API_HASH = "f69b703d137c22fdf39635718b61185b";
    @Autowired
    private RestTemplate restTemplate;


    public List<Character> getAllCharacters() {
        String url = createEndpoint("characters");

        ResponseEntity<CharactersResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                CharactersResponse.class
        );

        CharactersResponse response = responseEntity.getBody();
        if (response != null && response.getData() != null) {
            return response.getData().getResults();
        } else {
            return Collections.emptyList();
        }
    }

    public Character getCharacterById(int characterId) {
        String url = createEndpoint("characters/" + characterId);

        ResponseEntity<CharactersResponse> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                CharactersResponse.class
        );

        CharactersResponse response = responseEntity.getBody();

        if (response != null && response.getData() != null) {
            List<Character> results = response.getData().getResults();
            if (!results.isEmpty()) {
                return results.get(0);
            }
        }
        return null;
    }

    public String createEndpoint(String path) {
        StringBuilder url = new StringBuilder(BASE_URL);
        url.append(path);
        url.append("?apikey=");
        url.append(API_KEY);
        url.append("&ts=1");
        url.append("&hash=");
        url.append(API_HASH);
        return url.toString();
    }
}