package com.apimarvel.apimarvel;

import com.apimarvel.apimarvel.model.ApiRequest;
import com.apimarvel.apimarvel.repository.ApiRequestRepository;
import com.apimarvel.apimarvel.service.MarvelApiService;

import com.dev.marvel.model.Character;
import com.dev.marvel.model.CharactersData;
import com.dev.marvel.model.CharactersResponse;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.dev.marvel.service.MarvelApiClient;
@SpringBootTest
public class ApiMarvelApplicationTests {

	@Mock
	private MarvelApiClient marvelApiClient;

	@Mock
	private ApiRequestRepository apiRequestRepository;

	@InjectMocks
	private MarvelApiService marvelApiService;

	@Test
	public void testGetAllCharacters() {
		// Arrange
		List<Character> characters = new ArrayList<>();
		characters.add(new Character(1011334, "3-D Man", "", "2014-04-29T14:18:17-0400", "http://gateway.marvel.com/v1/public/characters/1011334"));
		characters.add(new Character(1009144, "A.I.M.", "AIM is a terrorist organization bent on destroying the world.", "2013-10-17T14:41:30-0400", "http://gateway.marvel.com/v1/public/characters/1009144"));

		CharactersResponse response = new CharactersResponse();
		CharactersData charactersData = new CharactersData();
		charactersData.setResults(characters);
		response.setData(new CharactersData());


		when(marvelApiClient.getAllCharacters()).thenReturn(characters);

		// Act
		List<Character> result = marvelApiService.getAllCharacters();

		// Assert
		assertEquals(characters, result);
		verify(marvelApiClient, times(1)).getAllCharacters();
		verify(apiRequestRepository, times(1)).save(any(ApiRequest.class));
	}

	@Test
	public void testGetCharacterById() {
		// Arrange
		int characterId = 1011334;
		Character character = new Character(characterId, "3-D Man", "", "2014-04-29T14:18:17-0400", "http://gateway.marvel.com/v1/public/characters/1011334");

		when(marvelApiClient.getCharacterById(characterId)).thenReturn(character);

		// Act
		Character result = marvelApiService.getCharacterById(characterId);

		// Assert
		assertEquals(character, result);
		verify(marvelApiClient, times(1)).getCharacterById(characterId);
		verify(apiRequestRepository, times(1)).save(any(ApiRequest.class));
	}
}