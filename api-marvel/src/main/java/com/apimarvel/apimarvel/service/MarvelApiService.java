package com.apimarvel.apimarvel.service;

import com.apimarvel.apimarvel.model.ApiRequest;
import com.apimarvel.apimarvel.repository.ApiRequestRepository;

import com.dev.marvel.model.Character;
import com.dev.marvel.service.MarvelApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MarvelApiService {
    @Autowired
    private ApiRequestRepository apiRequestRepository;
    @Autowired
    private MarvelApiClient marvelApiClient;

    public List<Character> getAllCharacters() {
        saveRequestTime();

        return marvelApiClient.getAllCharacters();
    }

    public Character getCharacterById(int characterId) {
        saveRequestTime();

        return marvelApiClient.getCharacterById(characterId);
    }
    private void saveRequestTime() {
        ApiRequest apiRequest = new ApiRequest();
        apiRequest.setRequestTime(LocalDateTime.now());

        apiRequestRepository.save(apiRequest);
    }
}