package com.apimarvel.apimarvel.service;

import com.apimarvel.apimarvel.model.ApiRequest;
import com.apimarvel.apimarvel.repository.ApiRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoggerService {
    @Autowired
    ApiRequestRepository apiRequestRepository;
    public List<ApiRequest> getAllRequests(){
        return apiRequestRepository.findAll();
    }
}
