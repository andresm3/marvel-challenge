package com.apimarvel.apimarvel.controller;

import com.apimarvel.apimarvel.service.MarvelApiService;
import com.dev.marvel.model.Character;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/marvel")
public class MarvelApiController {
    @Autowired
    private MarvelApiService marvelApiService;

    @GetMapping("/characters")
    public List<Character> getAllCharacters() {
        return marvelApiService.getAllCharacters();
    }

    @GetMapping("/characters/{characterId}")
    public Character getCharacterById(@PathVariable int characterId) {
        return marvelApiService.getCharacterById(characterId);
    }
}