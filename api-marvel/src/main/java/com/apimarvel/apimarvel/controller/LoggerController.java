package com.apimarvel.apimarvel.controller;

import com.apimarvel.apimarvel.model.ApiRequest;
import com.apimarvel.apimarvel.service.LoggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/logger")
public class LoggerController {
    @Autowired
    LoggerService loggerService;
    @GetMapping("")
    public List<ApiRequest> getAllCharacters() {
        return loggerService.getAllRequests();
    }

}
