package com.apimarvel.apimarvel.repository;

import com.apimarvel.apimarvel.model.ApiRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiRequestRepository extends JpaRepository<ApiRequest, Long> {

}