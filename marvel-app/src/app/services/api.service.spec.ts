import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { Character } from '../model/character.model';

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should retrieve all characters', () => {
    const mockCharacters: Character[] = [
      { 
        id: 1,
        name: 'Character 1',
        description: 'Description 1',
        modified: '2021-01-01T00:00:00Z',
        resourceURI: 'http://example.com/characters/1'
      },
      { 
        id: 2,
        name: 'Character 2',
        description: 'Description 2',
        modified: '2021-01-02T00:00:00Z',
        resourceURI: 'http://example.com/characters/2'
      }
    ];

    service.getAllCharacters().subscribe((characters: Character[]) => {
      expect(characters.length).toBe(2);
      expect(characters).toEqual(mockCharacters);
    });

    const req = httpMock.expectOne('your-api-endpoint/characters');
    expect(req.request.method).toBe('GET');
    req.flush(mockCharacters);
  });

  it('should retrieve a character by ID', () => {
    const mockCharacter: Character = { 
      id: 1,
      name: 'Character 1',
      description: 'Description 1',
      modified: '2021-01-01T00:00:00Z',
      resourceURI: 'http://example.com/characters/1'
    };

    service.getCharacterById(1).subscribe((character: Character) => {
      expect(character).toEqual(mockCharacter);
    });

    const req = httpMock.expectOne('your-api-endpoint/characters/1');
    expect(req.request.method).toBe('GET');
    req.flush(mockCharacter);
  });
});
