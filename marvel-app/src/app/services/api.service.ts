import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private baseUrl = 'http://localhost:8080/api'; // Replace with your API base URL

  constructor(private http: HttpClient) {}

  getAllCharacters(): Observable<any> {
    const url = `${this.baseUrl}/characters`;
    return this.http.get<any>(url);
  }

  getCharacterById(characterId: number): Observable<any> {
    const url = `${this.baseUrl}/characters/${characterId}`;
    return this.http.get<any>(url);
  }

  getQueryLog(): Observable<any[]> {
    const url = `${this.baseUrl}/query-log`;
    return this.http.get<any[]>(url);
  }
}
