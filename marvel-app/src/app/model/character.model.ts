export class Character {
    id: number;
    name: string;
    description: string;
    modified: string;
    resourceURI: string;
  
    constructor(id: number, name: string, description: string, modified: string, resourceURI: string) {
      this.id = id;
      this.name = name;
      this.description = description;
      this.modified = modified;
      this.resourceURI = resourceURI;
    }
  }
  