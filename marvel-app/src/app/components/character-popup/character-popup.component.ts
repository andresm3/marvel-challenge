import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Character } from 'src/app/model/character.model';

@Component({
  selector: 'app-character-popup',
  templateUrl: './character-popup.component.html',
  styleUrls: ['./character-popup.component.css']
})
export class CharacterPopupComponent {
  @Input() character!: Character;
  @Output() close = new EventEmitter<void>();
}
