import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-query-log',
  templateUrl: './query-log.component.html',
  styleUrls: ['./query-log.component.css']
})
export class QueryLogComponent {
  queryLog: any[] = [];

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.apiService.getQueryLog().subscribe(response => {
      this.queryLog = response;
    });
  }
}
