

import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Character } from 'src/app/model/character.model';

@Component({
  selector: 'app-root',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {
  characters: Character[] = [];
  selectedCharacter: Character | null = null;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.getCharacters();
  }
   

  getCharacters() {
    this.apiService.getAllCharacters().subscribe({
      next: (response: Character[]) => {
        this.characters = response;
      },
      error: (error) => {
        console.log('Error:', error);
      }
    });
  }

  selectCharacter(character: Character) {
    this.selectedCharacter = character;
  }

  closePopup() {
    this.selectedCharacter = null;
  }
}
